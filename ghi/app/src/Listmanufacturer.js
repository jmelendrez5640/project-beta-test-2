import React, { useState, useEffect } from "react";

function Listmanufacturer() {
  const [manufacturers, setManufacturers] = useState([]);

  async function loadManufacturers() {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No manufacturers found", e);
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);
  return (
    <>
      <div className="container">
        <h1>Manufacturers</h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default Listmanufacturer;
