from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length=250)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17, null=True)
    customer = models.CharField(max_length=200, null=True)
    vip = models.BooleanField(blank=True, default=False, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"Appointment with {self.technician} on {self.date_time}"
